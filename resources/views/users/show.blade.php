@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Update user') }}</div>

                    <div class="card-body">
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['name']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="surname" class="col-md-4 col-form-label text-md-end">{{ __('Surname') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['surname']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['email']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="birth_date" class="col-md-4 col-form-label text-md-end">{{ __('Birth Date') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['birth_date']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="south_african_id" class="col-md-4 col-form-label text-md-end">{{ __('South African Id Number') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['south_african_id']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="mobile_number" class="col-md-4 col-form-label text-md-end">{{ __('Mobile Number') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['mobile_number']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="language" class="col-md-4 col-form-label text-md-end">{{ __('Language') }}</label>

                                <div class="col-md-6">
                                    <p>{{$user['language']}}</p>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="language" class="col-md-4 col-form-label text-md-end">{{ __('Interests') }}</label>


                                <div class="col-md-6 dropdown-check-list list1" tabindex="100">
                                    <p>
                                        @foreach($user->interests as $interest)
                                            {{$interest['name']}},
                                        @endforeach
                                    </p>
                                </div>
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <a type="submit" class="btn btn-primary" href="{{route('users.index')}}">
                                        {{ __('Back') }}
                                    </a>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
