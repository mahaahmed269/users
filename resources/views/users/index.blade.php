@extends('layouts.app')

@section('content')
    <div class="container relative text-center flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        @if(Session::has('message'))
            @foreach(Session::get('message') as $class => $message)
                <p class="alert {{ $class}}">{{$message}}</p>
            @endforeach
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <a class="btn text-success" href="{{route('users.create')}}"><u>{{__('New User')}}</u></a>
        <table class="table">
            <thead class="table-dark">
                <tr>
                    <td>{{__('ID')}}</td>
                    <td>{{__('Name')}}</td>
                    <td>{{__('Surname')}}</td>
                    <td>{{__('Operations')}}</td>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <th>{{$user->id}}</th>
                        <th>{{$user->name}}</th>
                        <th>{{$user->surname}}</th>
                        <th>
                            <a class="text-primary btn" href="{{route('users.show',['user'=>$user->id])}}">{{__('Show')}}</a>
                            <a class="text-success btn" href="{{route('users.edit',['user'=>$user->id])}}">{{__('Edit')}}</a>

                            <a class="text-danger btn delete"
                            >{{__('Delete')}}</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">{{ __('Delete') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ __('Are you sure you want to delete tis user?') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal" aria-label="Close" data-bs-dismiss="modal">{{ __('Cancel') }}</button>
                    <button type="button" class="btn btn-primary"
                            onclick="event.preventDefault(); document.getElementById('delete-form').submit();">{{ __('Delete') }}</button>
                </div>
            </div>
        </div>

        <form method="post" class="d-none" id="delete-form" action="{{route('users.index')}}">
            @csrf
            @method('delete')
            <button type="submit"></button>
        </form>


    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        $('tbody').on('click', '.delete', function () {
            let id = $(this).parent().siblings()[0].innerHTML
            let action = '{{ route("users.destroy", ["user" => ":id"]) }}';
            action = action.replace(':id', id)
            console.log('action',action)
            $('#delete-form').attr('action', action);
            $('#deleteModal').modal('show');
        });

    </script>
@endsection
