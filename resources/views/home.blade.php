@extends('layouts.app')

@section('content')
    <div class="relative text-center flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        <a href="{{route('users.index')}}"><u>{{__('Manage Users')}}</u></a>
    </div>
@endsection
