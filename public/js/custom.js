let interests = $(".interests-count").html();

function toggle_visibility(el) {
    el.toggleClass("visible");
}

$(".list1 .anchor").click(function () {
    toggle_visibility($(".list1"));
});
$(".interest").click(function () {
    if ($(this).prop("checked") === true) {
        interests++;
        console.log('interests', interests);
    } else {
        interests--;
        console.log('interests', interests);
    }
    $(".interests-count").html(interests);
});
