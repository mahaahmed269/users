<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserFormRequest;
use App\Http\Requests\UpdateUserFormRequest;
use App\Models\Interest;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index')->with(['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create')->with(['interests' => Interest::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserFormRequest $request)
    {
        $user = User::create($request->validated());
        if (array_key_exists('interests', $request->validated())) {
            $user->interests()->attach($request['interests'], [
                'created_at' => now()
            ]);
        }
        Session::flash('message', ['alert-success' => __('User created successfully')]);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('users.show')->with(['user' => User::query()->where('id', $id)->with('interests')->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('users.edit')->with(['user' => User::query()->where('id', $id)->with('interests')->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserFormRequest $request, $id)
    {
        $data = $request->validated();
        $user = User::query()->where('id', $id)->first();
        if (array_key_exists('interests', $data)) {
            $user->interests()->sync($request['interests'], [
                'created_at' => now()
            ]);
            unset($data['interests']);
        }
        $user->update($data);
        Session::flash('message', ['alert-success' => __('User updated successfully')]);
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::query()->find($id)->delete();
        Session::flash('message', ['alert-success' => __('User Deleted Successfully')]);
        return redirect()->route('users.index');
    }
}
