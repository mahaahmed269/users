<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'south_african_id' => ['required', 'string'],
            'mobile_number' => ['required', 'string'],
            'birth_date' => ['required', 'string'],
            'language' => ['required', 'string'],
            'interests' => ['array','nullable'],
        ];
    }
}
